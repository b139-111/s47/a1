import {Card, Button, Container} from 'react-bootstrap'
const Highlights = () => {
  return (
    <Container fluid>
   <Card style={{ width: '18rem' }} className="highlightCard p-3">
  <Card.Body>
    <Card.Title>Card Title</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
    <Button variant="primary">Go somewhere</Button>
  </Card.Body>
</Card>
</Container>


    )
}
export default Highlights;