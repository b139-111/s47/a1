import {useState, useEffect} from "react"
import {
	Container, 
	Button, 
	Row, 
	Form, 
	Col
} from 'react-bootstrap'

export default function Register(){
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword, setCp] = useState("")
	const [isDisabled, setisDisabled] = useState(true);


	function registerUser(e){
		e.preventDefault()
		setEmail("")
		setPassword("")
		setCp("")

		alert("Thanks for registering")
	}
	useEffect( () => {
		if ((email != "" && password != "" && confirmPassword != "") && ( password == confirmPassword)){
			setisDisabled(false)
		}
	}, [email, password, confirmPassword])
	return(

		<Container className = "my-5">
			<Row className = "justify-content-center">
				<Col xs ={10} md= {6}>
					<Form 
					className = "border p-3" 
					onSubmit={(e) =>{registerUser(e)}
					}>

					{/*email*/}
					  <Form.Group className="mb-3" controlId="formBasicEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
						    type="email" 
						    placeholder="Enter email" 
						    value={email}
						    onChange={ (e) => {
						    	//console.log(e.target.value)
						    	setEmail(e.target.value)
						    }}


					    />
					    <Form.Text className="text-muted">
					      We'll never share your email with anyone else.
					    </Form.Text>
					  </Form.Group>
					  {/*pass*/}
					  <Form.Group className="mb-3" controlId="formBasicPassword">
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
						    type="password" 
						    placeholder="Password" 
						    value={password}
						    onChange={(e) => (
						    	setPassword(e.target.value)
						    )}
					    />
					  </Form.Group>
					  {/*confirm pass*/}
					  <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
					    <Form.Label>Confirm Password</Form.Label>
					    <Form.Control 
						    type="password" 
						    placeholder="Confirm Password" 
						    value = {confirmPassword}
						    onChange={(e) => (
						    	setCp(e.target.value)
						    )}

					    />
					  </Form.Group>

					  <Button variant="primary" type="submit" disabled={isDisabled}>
					    Submit
					  </Button>
					</Form>

				</Col>
			</Row>
		</Container>
	
	)
}